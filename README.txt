CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Using module
 * Decisions


INTRODUCTION
------------

Current Maintainer: Vlad Savitsky <vlad.savitsky@gmail.com>

Module allows to perform simple search by keyword in node's title, body
or summary. Search results and search form are shown at the same page.

User could also search using URLs like this:
http://example.com/mysearch/<search term>

Search results will be shown as unordered list by default but user could
switch to more attractive table layout where node's summary also shown and
search term highlighted.

Module was created during coding interview in Blink Reaction company.


INSTALLATION
------------

1. Place module into your Drupal modules directory (normally sites/all/modules).
2. Enable module.
   1. Using web-browser.
      - Open Modules page in web browser (/admin/modules)
      - Enable 'Swank Search' module.
   2. Using drush.
      - drush en swank_search -y
3. Open 'Permissions page' ('admin/people/permissions') and
   choose which user roles should have access to search form and search results.

Done! Module installed and condigured.


USING MODULE
-------------

To start using module open 'mysearch/' URL in web-browser.

By default you will see all published nodes in list (no filtering).
You could filter using 'search term'. Search term will be searched in node's
title, body or body summary. Table layout also highlights found search
terms in title or body summary.

There will be only 9 items per page by default but you could change this
number using dropdown list. If you need to see all search results at one
page you should select 'Show all items'.

To switch layout to 'Table layout' you should check 'Table layout' checkbox and
press 'Filter' button. Table layout shows search results in more attractive way.
There are number of item in search result, highlighting search term, extra column
for node type, ability to sort results and more.

To reset all filters you should click 'Reset' link.


DECISIONS
----------------

* I decided to add search form to page because it's not user friendly to force
  user edit URL each time he wants to search. But yes user stil could use URL to
  add or change search term. For example, http://example.com/mysearch/book will
  search for term 'book' in all published nodes.
* Textfield will have focus each time page loads so user could start typing
  search terms and start search by simple (and fast) press Enter button.
* Search results are shown as unordered list by default by more user friendly
  table layout was implemented as an option.
* Added ability to choose number of items per page. Users with fast internet
  connection could see all results at one page but others could review search
  results in small pieces.
* Original module was renamed from 'mysearch' to 'swank_search' just for fun.
* Fixed various security issues.
* Code was formatted to meet Drupal Coding Standards requirements.
* Outdated code was replaced using modern Drupal 7 API.
* Added .info file to be able to use module in drupal.
* Added .css file to style filter form and place all elements in one row.
* Added comments to each function and where it's necessary.
* Added pager to splite huge list to smaller pieces.
* Number of items per page in the list was selected to show off.
* All filter values are present in URL to have ability to share links to
  search results.
* Setting focus to textfield was implemented as inline javascript because there
  is no reason to have separate JS-file for one-line code.
* Loading whole nodes to build search results was replaces with smart sql-queries
  which get only necessary information.
* All other changes, improvements and bugs was done just for fun :)

